<?php 
require_once ("./code.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S02 Activity</title>
</head>
<body>
    <h1>Divisible by Five</h1>
    <?= getDivisibleByFive(); ?>

    <h1>Array Manipulation</h1>
    <?php
    $students = [];
    array_push($students, 'John Smith'); ?>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students); ?></p>
    
    <?php    
    array_push($students, 'Jane Smith'); 
    ?>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students); ?></p>

    <?php    
    array_shift($students); 
    ?>
    <p><?= var_dump($students); ?></p>
    <p><?= count($students); ?></p>

    
    
    
    
</body>
</html>