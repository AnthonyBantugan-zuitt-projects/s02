<?php 
require_once "./code.php" 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S02</title>
</head>
<body>
    <h1>Repetition Control</h1>
    <h3>While Loop</h3>
    <?php whileLoop(); ?>
    <h3>Do-While Loop</h3>
    <?php doWhileLoop(); ?>    
    <h3>For Loop</h3>
    <?php ForLoop(); ?>

    <h1>Array Manipulation</h1>
    
    <h2>Types of Arrays</h2>

    <h3>Simple Array</h3>
    <ul>
        <?php foreach($grades as $grade){?>
            <li><?php echo $grade; ?></li>
        <?php } ?>
    </ul>

    <h3>Associative Array</h3>
    <ul>
        <?php foreach($gradePeriods as $period => $grade){?>
            <li>Grade in <?= $period; ?> is <?= $grade; ?></li>
        <?php } ?>
    </ul>

    <h3>Multi Dimentional Array</h3>
    <ul>
        <?php 
            foreach($heroes as $team){
                foreach($team as $member){
                    ?>
                    <li><?= $member; ?></li>
                    <?php }
            }
        ?>
    </ul>

    <h3>Multi Dimentional Associative Array</h3>
    <?php 
        foreach($powers as $label => $powerGroup){
            foreach($powerGroup as $power){
                ?>
                <li><?= "$label: $power"; ?></li>
            <?php }
        }
    ?>

    <h1>Array Functions</h1>
    <?php array_push($computerBrands, 'Apple'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_unshift($computerBrands, 'Dell'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Remove from Array</h3>

	<?php array_pop($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Sort/Reverse</h3>

	<?php sort($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php rsort($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Count</h3>
	<p><?= count($computerBrands); ?></p>

	<h3>In Array</h3>
	<p><?= searchBrand('HP', $computerBrands); ?></p>


</body>
</html>